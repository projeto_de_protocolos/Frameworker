#include <iostream>
#include <cassert>
#include <stdexcept>
#include <stdio.h>
#include <string.h>
#include "frameworker.h"

using namespace std;

extern "C"{
    void frame_dump(char * buffer, int len) {
        int m = 0, line = 0;
        while (m < len) {
            printf("%02X: ", line*16);
            for (int n=0; n < 16 and m < len; n++, m++) {
                int x = (unsigned char)buffer[m];
                printf("%02X ", x);
            }
            puts("");
            line++;
        }        
    }
}

Frameworker::Frameworker(Serial & dev, int bytes_min, int bytes_max) : port(dev) {
    min_bytes = bytes_min;
    max_bytes = bytes_max;
    n_bytes = 0;
    state = IDLE;
}

Frameworker::~Frameworker() {}

void Frameworker::_configure_select(int sec, int usec) {
    fd = port.get();
    FD_ZERO(&read_fds);
    FD_SET(fd, &read_fds);

    timeout.tv_sec = sec;
    timeout.tv_usec = usec;
}

void Frameworker::send(char * send_buffer, int bytes) {
	if (bytes < min_bytes || bytes > max_bytes) {
		throw exception();
	}

	char frame[33] = {0};
	char temp[32] = {0};
	memcpy(temp, send_buffer, bytes);

	gen_crc(temp, bytes);
	bytes += 2;	
    int new_bytes = bytes;

	frame[0] = 0x7E;
	int pos = 1;
	for (int i = 0; i < new_bytes; i++) {
		if (temp[i] == 0x7E or temp[i] == 0x7D) {
			frame[pos++] = 0x7D;
			frame[pos++] = temp[i] xor 0x20;
            bytes++;
		}
        else {
            frame[pos++] = temp[i];
        }
	}
	frame[pos++] = 0x7E;
    bytes += 2;

	int _write_return = port.write(frame, bytes);

	if(_write_return == 0) {
        //printf("Failed to send ");
        frame_dump(frame, bytes);
    }
	else {
        //printf("Success to send ");
        frame_dump(frame, bytes);
    }
}

bool Frameworker::handle(char byte) {
    switch (state) {
        case IDLE:
            if (byte == 0x7E) {
                n_bytes = 0;
                state = RX;
            }
            else {
                state = IDLE;
            }
            break;
        case RX: if (byte == 0x7D) {
                state = ESC;
                break;
            }
            if (byte == 0x7E) {
                state = IDLE;
                if (n_bytes < min_bytes) {
                    cout << "Lenght of frame less than min_bytes." << endl;
                    n_bytes = 0;
                    break;
                }
                return true;
            }
            else {
                if (n_bytes > max_bytes) {
                    cout << "Lenght of frame greater than max_bytes." << endl;
                    n_bytes = 0;
                    state = IDLE;
                    break;
                }
                buffer[n_bytes] = byte;
                state = RX;
                n_bytes++;
                break;
            }
        case ESC:
            if ((byte != 0x7D) && (byte != 0x7E)) {
                buffer[n_bytes] = (byte xor 0x20);
                n_bytes = n_bytes + 1;
                state = RX;
            }
            else {
                cout << "Error during escape operation" << endl;
                n_bytes = 0;
                state = IDLE;
            }
            break;
    }
    return false;
}

int Frameworker::recv(char * buffer_out) {
    while (true) {
        char byte;
        _configure_select(1, 0);
        if(select(fd+1, &read_fds, NULL, NULL, &timeout)) {
            // port.flush();
            port.read(&byte, 1, true);
            if(handle(byte)) {
                if(check_crc(buffer, n_bytes)) {
                    char frame[n_bytes-2];
                    memcpy(frame, buffer, n_bytes-2);
                    memcpy(buffer_out, frame, n_bytes);
                    return n_bytes-2;
                }
            else {
                    return -1;
                }
            }
        }
        else {
            state = IDLE;
        }
    }
}

int Frameworker::recv(char * buffer_out, int sec, int usec) {
    bool wait = true;
    //printf("DEBUG:Frameworker Inside recv with parameters.\n");
    while (wait) {
        char byte;
        if(sec > 0 || usec > 0) {
            _configure_select(sec, usec);
        }
        if(select(fd+1, &read_fds, NULL, NULL, &timeout)) {
            // port.flush();
            port.read(&byte, 1, true);
            // port.read(&byte, 1, true);
            if(handle(byte)) {
                if(check_crc(buffer, n_bytes)) {
                    char frame[n_bytes-2];
                    memcpy(frame, buffer, n_bytes-2);
                    memcpy(buffer_out, frame, n_bytes);
                    return n_bytes-2;
                }
                else {
                    return -1;
                }
            }
        }
        else {
            state = IDLE;
            wait = false;
        }
    }
    return -2;
}

bool Frameworker::check_crc(char * buffer, int len) {
    uint16_t trialfcs = pppfcs16(PPPINITFCS16, buffer, len);
    if (trialfcs == PPPGOODFCS16) {
        return true;
    }
    return false;
}
  
void Frameworker::gen_crc(char * buffer, int len){
    uint16_t value = pppfcs16(PPPINITFCS16, buffer, len);
    value ^= 0xffff;                
    buffer[len] = (value & 0x00ff);     
    buffer[len+1] = ((value >> 8) & 0x00ff);
}

uint16_t Frameworker::pppfcs16(uint16_t fcs, char * cp, int len) { 
    assert(sizeof (uint16_t) == 2);
    assert(((uint16_t) -1) > 0);
    while (len--) {
        fcs = (fcs >> 8) ^ fcstab[(fcs ^ *cp++) & 0xff];
    }
    return (fcs);
}
