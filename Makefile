TX_OBJS = test/arp_transmitter_test.cc lib/Serial.cc frameworker.cc arq.cc
RX_OBJS = test/arp_receiver_test.cc lib/Serial.cc frameworker.cc arq.cc
BO_OBJS = test/arp_backoff_test.cc lib/Serial.cc frameworker.cc arq.cc

CC = g++

COMPILER_FLAGS = -std=c++11 -g

# LIBS = -lz -lrt -lm
LIBS = -lz -lm

OBJ_TX_NAME = tx_protocol_test
OBJ_RX_NAME = rx_protocol_test
OBJ_BO_NAME = bo_protocol_test

all : $(OBJS)
	$(CC) $(TX_OBJS) $(COMPILER_FLAGS) $(LIBS) -o $(OBJ_TX_NAME)
	$(CC) $(RX_OBJS) $(COMPILER_FLAGS) $(LIBS) -o $(OBJ_RX_NAME)
	$(CC) $(BO_OBJS) $(COMPILER_FLAGS) $(LIBS) -o $(OBJ_BO_NAME)

clean:
	rm -f $(OBJ_TX_NAME) $(OBJ_RX_NAME) $(OBJ_BO_NAME) *.o
