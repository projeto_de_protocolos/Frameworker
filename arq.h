#ifndef ARQ_H
#define ARQ_H

#include "frameworker.h"
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <iostream>
#include <cstring>

class Arq {
    private:
        enum States {
            IDLE, // IDLE means the Arq is read to send new message
            BUSY,  // BUSY means the Arq is waiting for a ack
            BACKOFF
        };
        int state;
        bool sequency;
        char last_msg[1024] = {0};
        bool ack;
        bool frame;
        int len_last_msg;
        int backoff_time;
        Frameworker & framer;
        void resend();
        void recved_frame(char * buffer, int len);	

        enum EventType {
            Payload,
            Frame,
            Ack,
        };
        struct Event {
            EventType type;
            char * ptr;
            int bytes;
            Event(EventType t, char * p, int len) : type(t), ptr(p), bytes(len) {}
        };
        bool handle(Event & e);
        void _manage_backoff(int backoff_time);

    public:
        int add_header(char * msg, int bytes, int type);
        int rm_header(char * msg, int bytes);
        Arq(Frameworker & frameworker);
        ~Arq();
        void send_frame(char * buffer, int len);
        void send_msg(char * buffer, int len);
        void send_ack();
        int recv(char * buffer);
        int recv(char * buffer, int sec, int usec);
};
#endif
