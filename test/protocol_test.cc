#include <iostream>
//#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <errno.h>
#include "../frameworker.h"
 
using namespace std;
 
void dump(char * buffer, int len) {
   int m = 0, line = 0;
 
    while (m < len) {
        printf("%02X: ", line*16);
 
        for (int n=0; n < 16 and m < len; n++, m++) {
            int x = (unsigned char)buffer[m];
            printf("%02X ", x);
        }
        puts("");
        line++;
    }        
}
 
int main(int argc, char * argv[]) {
    if(argv[1] == NULL) {
        printf("Please, pass the path of the serial port in the command line.\n");
        return -1;
    }

    char * path = argv[1];
    printf("Creating serial connection...\n");
    Serial dev(path, B9600);
    printf("Creating frameworker instance...\n");
    Frameworker proto(dev, 8, 32);
    char frame[32];
    // char * frame;

    printf("Sending data Frameworker...\n");
    try {
        proto.send("1234567890", 10);
    }
    catch (int e) {
            printf("Standard exception: %d\n", e);
    }

    int bytes = proto.recv(frame);
    printf("Received %d!\n", bytes);
    dump(frame, bytes);
}
