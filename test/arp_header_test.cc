#include <iostream>
//#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <errno.h>
#include "../arq.h"
#include "../frameworker.h"
 
using namespace std;

void _dump_buffer(char * buffer, int len) {
    int m = 0, line = 0;
    while (m < len) {
        printf("%02X: ", line*16);
        for (int n=0; n < 16 and m < len; n++, m++) {
            int x = (unsigned char)buffer[m];
            printf("%02X ", x);
        }
        puts("");
        line++;
    }        
}
 
int main(int argc, char * argv[]) {
    if(argv[1] == NULL) {
        printf("Please, pass the path of the serial port in the command line.\n");
        return -1;
    }

    char * path = argv[1];
    printf("Creating serial connection...\n");
    Serial dev(path, B9600);
    printf("Creating frameworker instance...\n");
    Frameworker proto(dev, 8, 32);
    Arq arq_tst(proto);
    int bytes = 10;
    char frame[bytes] = "1234567890";

    printf("Adding header to %s using arq...\n", frame);
    try {
        bytes = arq_tst.add_header(frame, bytes, 0);
    }
    catch (int e) {
            printf("Standard exception: %d\n", e);
    }
    cout << "Frame with Arq is: " << endl;
    _dump_buffer(frame, bytes);
    
    try {
        bytes = arq_tst.rm_header(frame, bytes);
    }
    catch (int e) {
            printf("Standard exception: %d\n", e);
    }

    cout << "Frame without Arq is: " << endl;
    _dump_buffer(frame, bytes);

}
