#include <iostream>
#include <fstream>

#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "../arq.h"
#include "../frameworker.h"
 
using namespace std;
 
void dump(char * buffer, int len) {
    int m = 0, line = 0;
    while (m < len) {
        printf("%02X: ", line*16);
        for (int n=0; n < 16 and m < len; n++, m++) {
            cout << "m is = " << m << endl;
            int x = (unsigned char)buffer[m];
            printf("%02X ", x);
        }
        puts("");
        line++;
    }        
}
 
int main(int argc, char * argv[]) {
    Serial dev(argv[1], B9600);
    char frame[32];
    memset(frame, 0, 32);
    Frameworker proto(dev, 8, 32);

    // First layer: ARQ
    proto.send("1234567890", 10);

    int bytes = proto.recv(frame);
    if (bytes == -1) {
        cout << "Error while receiving (-1)" << endl;
        cout << "Shit is " << frame << endl;
    }
    else {
        cout << "Received " << bytes << " bytes. DUMPINNNNGGGGG!!!" << endl;
        cout << frame << endl;
//        dump(frame, bytes);
    }
}
