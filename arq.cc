#include "arq.h"
#define _HEADER 2; 

using namespace std;

extern "C"{
    void arq_dump(char * buffer, int len) {
        int m = 0, line = 0;
        while (m < len) {
            printf("%02X: ", line*16);
            for (int n=0; n < 16 and m < len; n++, m++) {
                int x = (unsigned char)buffer[m];
                printf("%02X ", x);
            }
            puts("");
            line++;
        }        
    }
}

Arq::Arq(Frameworker & frameworker) : framer(frameworker) {
	state=IDLE;
	sequency=false;
	len_last_msg = 0;
    ack = false;
    frame = false;
}

Arq::~Arq() {
}

int Arq::add_header(char * msg, int bytes, int type) {
	char frame[32] = {0};
	char tmp[32] = {0};
	memcpy(tmp, msg, bytes);

	switch(type) {
        // In case it is a payload frame:
		case Payload:
			frame[0] = 0x11;
			break;
        // In case it is a ack frame:
		case Ack:
			frame[0] = 0x22;
			break;
	}
	
    // Here we add the sequency number (0 or 1) and than invert the sequency;
    // We need only two numbers of sequency for now because or protocol
    // is too simple. Working with this boolean logic will ease the protocol's
    // job
    frame[1] = sequency ? 0x00 : 0x01;
    sequency =! sequency;
	
    // Here we insert the payload to the frame with header
	int pos = _HEADER;
	for (int i = 0; i < bytes; i++) {
	     frame[pos++] = tmp[i];
	}

	bytes += _HEADER;	
    // Than, write the new frame to the buffer passed and return its length
	memcpy(msg, &frame, bytes);
	return bytes;
}

int Arq::rm_header(char * msg, int bytes) {
    // Method to rm_header the header from the frame, keeping only wanted bytes
	char frame[32] = {0};
	char temp[32] = {0};
	memcpy(temp, msg, bytes);

	int pos = 0;
    int i = _HEADER;
	for(i; i < bytes; i++) {
        frame[pos++] = temp[i];
	}

	memcpy(msg, frame, bytes);
	bytes -= _HEADER;	

	return bytes;
}

void Arq::resend(){
    //printf("DEBUG:Arq Resending message.\n");
    send_msg(last_msg, len_last_msg);
}

void Arq::recved_frame(char * buffer, int len){
    if(buffer[0] == 0x22) {
        // If it's a ACK frame:
        char temp_sequency = !sequency ? 0x00 : 0x01;
        if(buffer[1] == temp_sequency) {
            state = IDLE;
            frame = false;
            ack = true;
        }
        else {
            resend();
        }
    }
    else if(buffer[0]==0x11) {
        // If it's a frame
        char tmp[32] = {0};
        memcpy(tmp, buffer, len);
        int new_len = rm_header(tmp, len);
        memcpy(buffer, tmp, new_len);
        send_ack();
    }
}
void Arq::send_ack() {
    char ack_frame[32] = "ackframe";
    int len = add_header(ack_frame, 8, Ack);
    frame = true;
    ack = false;
    send_frame(ack_frame, len);
}

void Arq::_manage_backoff(int backoff_time) {
    //printf("DEBUG:Arq Inside backoff managment\n");
    char new_frame[32]={0};
    int len = recv(new_frame, backoff_time, 0);
    Event nframe(Frame, new_frame, len);
    handle(nframe);
}

bool Arq::handle(Event & e) {
// bool Arq::handle(char * buffer, int len, int type) {
    //printf("DEBUG:Arq Inside FSM. State is %d\n", state);
    switch (state) {
        case IDLE:
            if(e.type == Payload) {
                e.bytes = add_header(e.ptr, e.bytes, Payload);
                state = BUSY;
                ack = false;
                frame = false;
                send_frame(e.ptr, e.bytes);
                break;
            }
            else if(e.type == Frame) {
                state = IDLE;
                return(true);
            }

        case BUSY:
            if(e.type == Frame) {
                recved_frame(e.ptr, e.bytes);	
                if(ack == true) {
                    state = BACKOFF;
                    _manage_backoff(3);
                }
                else {
                    state = BUSY;			
                }
                break; 
            }
            else if(e.type == Payload) {
                printf("Sending message while waiting ack not implemented\n");
            }
        case BACKOFF:
            if(e.type == Frame && e.bytes > 0) {
                recved_frame(e.ptr, e.bytes);
                state = BACKOFF;
                _manage_backoff(0);
            }
            else {
                state = IDLE;
            }
    }
    return(false);
}

void Arq::send_msg(char * buffer, int len) {
    // save last message in case resend is needed
    memcpy(last_msg, buffer, len);
    len_last_msg = len;

    // method for application use, to send a message using the protocol
    char buffer_aux[32]={0};
    memcpy(buffer_aux, buffer, len);
    Event aux(Payload, buffer_aux, len);
    handle(aux);

    int max_resend = 3;
    while(!ack) {
        char ack_buf[32]={0};
        //printf("DEBUG:Ack Waiting ack inside send_msg()\n");
        len = recv(ack_buf, 1, 0);
        Event ack_e(Frame, ack_buf, len);
        handle(ack_e);
        max_resend--;
        if(max_resend == 0) {
            //printf("DEBUG:Arq Target can't receive message now\n");
            state = IDLE;
            break;
        }
    }
}

void Arq::send_frame(char * buffer, int len) {
// send the buffer to the frameworker
    char aux[32]={0};
    memcpy(aux, buffer, len);
    framer.send(aux, len);
    //printf("DEBUG:Arq Frame sent: ");
    arq_dump(aux, len);
    memcpy(buffer, aux, len);
}

int Arq::recv(char * buffer) {
    int bytes = framer.recv(buffer);
    Event new_event(Frame, buffer, bytes);
    if(handle(new_event)) {
        recved_frame(buffer, bytes);
    }
}

int Arq::recv(char * buffer, int sec, int usec) {
    int bytes = framer.recv(buffer, sec, usec);
    Event new_event(Frame, buffer, bytes);
    if(handle(new_event)) {
        recved_frame(buffer, bytes);
    }
}
